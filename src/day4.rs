use std::{collections::HashMap, num::ParseIntError};

fn check_doubles(number: u32) -> bool {
    let mut result = false;

    let string = number.to_string();
    let mut chars = string.chars().peekable();
    while let Some(curr) = chars.next() {
        let next = match chars.peek() {
            Some(c) => c,
            _ => break,
        };

        if &curr == next {
            result = true;
            break;
        }
    }

    result
}

fn check_increasing(number: u32) -> bool {
    let mut result = true;

    let mut last: Option<char> = None;
    let string = number.to_string();
    for c in string.chars() {
        if let Some(last) = last {
            if c < last {
                result = false
            }
        }

        last = Some(c);
    }

    result
}

fn check_doubles_strict(number: u32) -> bool {
    let mut last: Option<char> = None;
    let mut counter: HashMap<char, usize> = HashMap::new();
    let string = number.to_string();
    for c in string.chars() {
        if let Some(last) = last {
            if c == last {
                let entry = counter.entry(c).or_insert(1);
                *entry += 1;
            }
        }

        last = Some(c);
    }

    counter.iter().any(|(_, d)| *d == 2)
}

#[aoc_generator(day4)]
fn input_generator(input: &str) -> Result<(u32, u32), ParseIntError> {
    let range: Vec<u32> = input
        .trim()
        .split_terminator('-')
        .map(|s| s.parse())
        .collect::<Result<Vec<u32>, ParseIntError>>()?;

    Ok((
        *range.get(0).expect("missing range start"),
        *range.get(1).expect("missing range end"),
    ))
}

#[allow(clippy::trivially_copy_pass_by_ref)]
#[aoc(day4, part1)]
fn solve_part1(input: &(u32, u32)) -> u32 {
    let mut counter = 0;

    for number in input.0..=input.1 {
        if check_doubles(number) && check_increasing(number) {
            counter += 1;
        }
    }

    counter
}

#[allow(clippy::trivially_copy_pass_by_ref)]
#[aoc(day4, part2)]
fn solve_part2(input: &(u32, u32)) -> u32 {
    let mut counter = 0;

    for number in input.0..=input.1 {
        if check_doubles_strict(number) && check_increasing(number) {
            counter += 1
        }
    }

    counter
}
