use crate::shared::intcode::*;
use std::num::ParseIntError;

#[aoc_generator(day2)]
fn input_generator(input: &str) -> Result<Vec<isize>, ParseIntError> {
    input
        .split_terminator(',')
        .map(|i| i.parse::<isize>())
        .collect()
}

#[aoc(day2, part1)]
fn solve_part1(input: &[isize]) -> isize {
    // Initialize the virtual machine.
    let mut vm = IntcodeVm::new(input);

    // Restore its memory to its state before the fire.
    vm.set_memory(1, 12).unwrap();
    vm.set_memory(2, 2).unwrap();

    // Run the virtual machine.
    vm.run().unwrap();

    // Get the result.
    vm.get_memory(0).unwrap()
}

#[aoc(day2, part2)]
fn solve_part2(input: &[isize]) -> isize {
    const MAGIC_NUMBER: isize = 19_690_720;

    let base_vm = IntcodeVm::new(input);

    for noun in 0..100 {
        for verb in 0..100 {
            let mut vm = base_vm.clone();

            vm.set_memory(1, noun).unwrap();
            vm.set_memory(2, verb).unwrap();

            vm.run().unwrap();

            if vm.get_memory(0) == Ok(MAGIC_NUMBER) {
                return 100 * vm.get_memory(1).unwrap() + vm.get_memory(2).unwrap();
            }
        }
    }

    panic!("failed to find noun and verb");
}
