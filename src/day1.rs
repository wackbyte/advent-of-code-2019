use std::num::ParseIntError;

/// Calculate the fuel required to lift a component.
fn calculate_fuel(mass: u32) -> u32 {
    (mass / 3).saturating_sub(2)
}

/// Calculate the fuel required to lift a component,
/// the fuel required for that fuel, and so on.
fn calculate_fuel_full(mass: u32) -> u32 {
    let mut fuel = calculate_fuel(mass);
    let mut last = fuel;
    while last != 0 {
        last = calculate_fuel(last);
        fuel += last;
    }
    fuel
}

#[aoc_generator(day1)]
fn input_generator(input: &str) -> Result<Vec<u32>, ParseIntError> {
    input.lines().map(|l| l.parse::<u32>()).collect()
}

#[aoc(day1, part1)]
fn solve_part1(input: &[u32]) -> u32 {
    input.iter().map(|m| calculate_fuel(*m)).sum()
}

#[aoc(day1, part2)]
fn solve_part2(input: &[u32]) -> u32 {
    input.iter().map(|m| calculate_fuel_full(*m)).sum()
}
