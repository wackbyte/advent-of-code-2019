use std::collections::HashSet;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Copy, Clone)]
struct Trace {
    length: usize,
    direction: Direction,
}

type Point = (isize, isize);

fn input_to_wires(input: &[Vec<Trace>]) -> (Vec<Trace>, Vec<Trace>) {
    (
        input.get(0).expect("missing first wire").to_vec(),
        input.get(1).expect("missing second wire").to_vec(),
    )
}

fn wires_to_points(wires: (Vec<Trace>, Vec<Trace>)) -> (Vec<Point>, Vec<Point>) {
    (wire_to_points(&wires.0), wire_to_points(&wires.1))
}

fn points_to_sets(points: (Vec<Point>, Vec<Point>)) -> (HashSet<Point>, HashSet<Point>) {
    (
        points.0.iter().copied().collect::<HashSet<_>>(),
        points.1.iter().copied().collect::<HashSet<_>>(),
    )
}

fn wire_to_points(wire: &[Trace]) -> Vec<Point> {
    let mut points = Vec::new();

    let mut x = 0;
    let mut y = 0;
    for trace in wire {
        for _ in 1..=trace.length {
            match trace.direction {
                Direction::Up => y += 1,
                Direction::Down => y -= 1,
                Direction::Left => x -= 1,
                Direction::Right => x += 1,
            }
            points.push((x, y));
        }
    }

    points
}

fn manhattan_distance(a: Point, b: Point) -> usize {
    ((b.0 - a.0).abs() + (b.1 - a.1).abs()) as usize
}

fn points_distance(points1: &[Point], points2: &[Point], point: Point) -> usize {
    points1
        .iter()
        .position(|(a, b)| *a == point.0 && *b == point.1)
        .unwrap()
        + points2
            .iter()
            .position(|(a, b)| *a == point.0 && *b == point.1)
            .unwrap()
        + 2
}

#[aoc_generator(day3)]
fn input_generator(input: &str) -> (Vec<Point>, Vec<Point>) {
    let data = input
        .trim()
        .lines()
        .map(|l| {
            l.split_terminator(',')
                .map(|s| {
                    let mut chars = s.chars();

                    let direction = match chars.next() {
                        Some(c) => match c {
                            'U' => Direction::Up,
                            'D' => Direction::Down,
                            'L' => Direction::Left,
                            'R' => Direction::Right,
                            _ => panic!("invalid direction"),
                        },
                        None => panic!("missing direction"),
                    };

                    let length = {
                        let string = chars.as_str();

                        if string.is_empty() {
                            panic!("missing amount");
                        }

                        string.parse::<usize>().expect("invalid amount")
                    };

                    Trace { direction, length }
                })
                .collect::<Vec<Trace>>()
        })
        .collect::<Vec<Vec<Trace>>>();
    wires_to_points(input_to_wires(&data))
}

#[aoc(day3, part1)]
fn solve_part1(input: &(Vec<Point>, Vec<Point>)) -> usize {
    let (points1, points2) = input;
    let (set1, set2) = points_to_sets((*input).clone());

    set1.intersection(&set2)
        .map(|point| manhattan_distance((0, 0), *point))
        .min()
        .unwrap()
}

#[aoc(day3, part2)]
fn solve_part2(input: &(Vec<Point>, Vec<Point>)) -> usize {
    let (points1, points2) = input;
    let (set1, set2) = points_to_sets((*input).clone());

    set1.intersection(&set2)
        .map(|point| points_distance(&points1, &points2, *point))
        .min()
        .unwrap()
}
