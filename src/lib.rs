#![deny(
    clippy::all,
    missing_copy_implementations,
    missing_debug_implementations,
    rust_2018_idioms
)]

#[macro_use]
extern crate aoc_runner_derive;

mod day1;
mod day2;
mod day3;
mod day4;

mod shared;

aoc_lib! { year = 2019 }
