use std::{
    convert::{TryFrom, TryInto},
    error::Error,
    fmt::{Display, Formatter, Result as FmtResult},
    num::ParseIntError,
    str::FromStr,
};

/// An Intcode virtual machine.
#[derive(Debug, Clone)]
pub struct IntcodeVm {
    memory: Vec<isize>,
    pointer: usize,
    halted: bool,
}

impl IntcodeVm {
    pub fn new<D>(data: D) -> Self
    where
        D: Into<Vec<isize>>,
    {
        Self {
            memory: data.into(),
            pointer: 0,
            halted: false,
        }
    }

    pub fn memory(&self) -> &[isize] {
        &self.memory
    }

    pub fn pointer(&self) -> usize {
        self.pointer
    }

    pub fn halted(&self) -> bool {
        self.halted
    }

    pub fn opcode(&self) -> IntcodeResult<Opcode> {
        (self.get_memory(self.pointer)? as isize).try_into()
    }

    pub fn get_memory(&self, address: usize) -> IntcodeResult<isize> {
        self.memory
            .get(address)
            .copied()
            .ok_or(IntcodeError::InvalidAddress)
    }

    pub fn get_memory_by_pointer(&self, address: usize) -> IntcodeResult<isize> {
        self.get_memory(self.get_memory(address)? as usize)
    }

    pub fn set_memory(&mut self, address: usize, value: isize) -> IntcodeResult<()> {
        self.memory
            .get_mut(address)
            .map(|v| *v = value)
            .ok_or(IntcodeError::InvalidAddress)
    }

    pub fn set_memory_by_pointer(&mut self, address: usize, value: isize) -> IntcodeResult<()> {
        self.set_memory(self.get_memory(address)? as usize, value)
    }

    pub fn step(&mut self) -> IntcodeResult<bool> {
        let opcode = self.opcode()?;

        match opcode {
            Opcode::Add => self.set_memory_by_pointer(
                self.pointer + 3,
                self.get_memory_by_pointer(self.pointer + 1)?
                    + self.get_memory_by_pointer(self.pointer + 2)?,
            )?,
            Opcode::Multiply => self.set_memory_by_pointer(
                self.pointer + 3,
                self.get_memory_by_pointer(self.pointer + 1)?
                    * self.get_memory_by_pointer(self.pointer + 2)?,
            )?,
            Opcode::Halt => {
                if self.halted {
                    return Err(IntcodeError::AlreadyHalted);
                } else {
                    self.halted = true;
                }
            }
            _ => return Err(IntcodeError::UnknownOpcode),
        }

        self.pointer += opcode.size();

        Ok(!self.halted)
    }

    pub fn run(&mut self) -> IntcodeResult<()> {
        while self.step()? {}
        Ok(())
    }
}

impl FromStr for IntcodeVm {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let data: Vec<isize> = s
            .trim()
            .split(',')
            .map(|s| s.parse::<isize>())
            .collect::<Result<Vec<isize>, _>>()?;
        Ok(Self::new(data))
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum IntcodeError {
    InvalidAddress,
    InvalidPointer,
    AlreadyHalted,
    UnknownOpcode,
}

impl Display for IntcodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(
            f,
            "{}",
            match *self {
                Self::InvalidAddress => "invalid address",
                Self::InvalidPointer => "invalid pointer",
                Self::AlreadyHalted => "already halted",
                Self::UnknownOpcode => "unknown opcode",
            }
        )
    }
}

impl Error for IntcodeError {}

pub type IntcodeResult<T> = Result<T, IntcodeError>;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Opcode {
    Add,
    Multiply,
    Halt,
}

impl Opcode {
    pub fn size(self) -> usize {
        match self {
            Self::Add | Self::Multiply => 4,
            Self::Halt => 1,
        }
    }
}

impl From<Opcode> for isize {
    fn from(opcode: Opcode) -> Self {
        match opcode {
            Opcode::Add => 1,
            Opcode::Multiply => 2,
            Opcode::Halt => 99,
        }
    }
}

impl TryFrom<isize> for Opcode {
    type Error = IntcodeError;

    fn try_from(value: isize) -> IntcodeResult<Self> {
        match value {
            1 => Ok(Self::Add),
            2 => Ok(Self::Multiply),
            99 => Ok(Self::Halt),
            _ => Err(IntcodeError::UnknownOpcode),
        }
    }
}
